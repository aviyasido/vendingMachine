﻿namespace Vending
{
    public static class VendingLib
    {
        private static int _moneyEntered = 0;
        private static readonly List<int> coinsList = Enum.GetValues(typeof(Coins)).Cast<int>().Select(coin => (int)coin).ToList();
        private static readonly Dictionary<string, int> drinks = Enum.GetValues(typeof(Drinks)).Cast<Drinks>()
            .ToDictionary(key => key.ToString(), value => (int)value);
        private static readonly Dictionary<string, int> addings = Enum.GetValues(typeof(Additional)).Cast<Additional>()
            .ToDictionary(key => key.ToString(), value => (int)value);
        private static readonly List<Drinks> drinkList = Enum.GetValues(typeof(Drinks)).Cast<Drinks>().ToList();
        private static readonly List<Additional> additionList = Enum.GetValues(typeof(Additional)).Cast<Additional>().ToList();

        public static Status EnterMoney(int moneyEntered)
        {
            _moneyEntered += moneyEntered;
            return Status.MONEY_ENTERED_SUCCESSFULLY;
        }

        public static int GetCurrentBalance()
        {
            return _moneyEntered;
        }

        public static Status ReturnMoney()
        {
            _moneyEntered = 0;
            return Status.MONEY_RETURNED_TO_USER;
        }

        public static void ShowManu()
        {
            Console.WriteLine("Optional drinks are: ");
            Console.WriteLine(String.Join(Environment.NewLine, drinks));
            Console.WriteLine("Optional drinks are: ");
            Console.WriteLine(String.Join(Environment.NewLine, addings));
        }

        public static Status BuyProduct(Drinks drink, int num)
        {
            int drinkPrice = Convert.ToInt32(drink) * num;

            if (_moneyEntered < drinkPrice)
            {
                return Status.NOT_ENOUGH_MONEY;
            }

            _moneyEntered -= drinkPrice;

            return Status.PRODUCT_SUPPLIED;
        }

        public static Status BuyProduct(Drinks drink, int Dnum, Additional add, int Anum)
        {
            int drinkPrice = Convert.ToInt32(drink) * Dnum;
            drinkPrice = Convert.ToInt32(add) * Anum + drinkPrice;

            if (_moneyEntered < drinkPrice)
            {
                return Status.NOT_ENOUGH_MONEY;
            }

            _moneyEntered -= drinkPrice;

            return Status.PRODUCT_SUPPLIED;
        }

        public static Dictionary<string, int> GetDrinkMap()
        {
            return drinks;
        }

        public static Dictionary<string, int> GetAddingsMap()
        {
            return addings;
        }

        public static int GetDrinkPrice(string key)
        {
            int price = 0;
            if (key != null)
            {
                drinks.TryGetValue(key, out price);
            }
            return price;
        }

        public static int GetAdditionPrice(string key)
        {
            int price = 0;
            if (key != null)
            {
                addings.TryGetValue(key, out price);
            }

            return price;
        }

        public static Drinks MatchDrink(string key)
        {
            foreach (Drinks drink in drinkList)
            {
                if (drink.ToString().Equals(key)) return drink;
            }
            return new Drinks();
        }

        public static Additional MatchAddition(string key)
        {
            foreach (Additional add in additionList)
            {
                if (add.ToString().Equals(key)) return add;
            }
            return new Additional();
        }
    }
}