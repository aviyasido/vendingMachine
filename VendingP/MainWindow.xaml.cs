﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vending;

namespace VendingP
{
    public partial class MainWindow : Window
    {
        private int currentDrinkSum = 0;
        private int currentAddingSum = 0;
        public MainWindow()
        {
            InitializeComponent();

            InitialCoffeeOptions();
            InitialAdditionOptions();
            InitialAdditionCount();
            InitialDrinksCount();
            currentBalance.Text = GetCurrentBalance();
            coffeeOptionsCombo.SelectionChanged += GetCurrentSumDrinks;
            drinksCount.SelectionChanged += GetCurrentSumDrinks;
            additionOptionsCombo.SelectionChanged += GetCurrentSumAddings;
            additionsCount.SelectionChanged += GetCurrentSumAddings;
        }

        private void InitialCoffeeOptions()
        {
            Dictionary<string, int> drinkMap = VendingLib.GetDrinkMap();
            foreach (KeyValuePair<string, int> drinks in drinkMap)
            {
                coffeeOptionsCombo.Items.Add(drinks.Key);
            }
        }

        private void InitialAdditionOptions()
        {
            Dictionary<string, int> addingsMap = VendingLib.GetAddingsMap();
            foreach (KeyValuePair<string, int> adding in addingsMap)
            {
                additionOptionsCombo.Items.Add(adding.Key);
            }
        }

        private void InitialAdditionCount()
        {
            for (int i = 0; i < 1250; i++)
            {
                additionsCount.Items.Add(i);
            }
        }

        private void InitialDrinksCount()
        {
            for (int i = 0; i < 1250; i++)
            {
                drinksCount.Items.Add(i);
            }
        }

        private string GetCurrentBalance()
        {
            return "Your current balance is " + VendingLib.GetCurrentBalance().ToString();
        }

        private void GetCurrentSumDrinks(object sender, SelectionChangedEventArgs e)
        {
            if (coffeeOptionsCombo.SelectedItem != null && drinksCount.SelectedItem != null && int.Parse(drinksCount.SelectedItem.ToString()) > 0)
            {
                int sum = int.Parse(drinksCount.SelectedItem.ToString());
                currentDrinkSum = SetDrinkPriceToPay(coffeeOptionsCombo.SelectedItem.ToString(), sum);
            }
            else
            {
                currentDrinkSum = 0;
                priceToPay.Text = "Sum to pay is 0";
            }
        }


        private void GetCurrentSumAddings(object sender, SelectionChangedEventArgs e)
        {
            if (additionOptionsCombo.SelectedItem != null && additionsCount.SelectedItem != null)
            {
                int sum = int.Parse(additionsCount.SelectedItem.ToString());
                currentAddingSum = SetAddingPriceToPay(additionOptionsCombo.SelectedItem.ToString(), sum);
            }
            else
            {
                currentAddingSum = 0;
            }
        }


        private int SetDrinkPriceToPay(string product, int multi)
        {
            priceToPay.Text = "Sum to pay is " + (VendingLib.GetDrinkPrice(product) * multi + currentAddingSum);
            return VendingLib.GetDrinkPrice(product) * multi;
        }


        private int SetAddingPriceToPay(string product, int multi)
        {
            if (currentDrinkSum == 0) priceToPay.Text = "Sum to pay is 0";
            else priceToPay.Text = "Sum to pay is " + (VendingLib.GetAdditionPrice(product) * multi + currentDrinkSum);
            return VendingLib.GetAdditionPrice(product) * multi;
        }

        private void cancelProduct_Click(object sender, RoutedEventArgs e)
        {
            VendingLib.ReturnMoney();
            currentBalance.Text = GetCurrentBalance();
        }

        private void addMoney_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(MoneyAdded.Text))
            {
                MessageBox.Show(VendingLib.EnterMoney(int.Parse(MoneyAdded.Text)).ToString());
                currentBalance.Text = GetCurrentBalance();
                MoneyAdded.Text = "";
            }
        }

        private void addProduct_Click(object sender, RoutedEventArgs e)
        {
            if (coffeeOptionsCombo.SelectedItem != null && drinksCount.SelectedItem != null && int.Parse(drinksCount.SelectedItem.ToString()) > 0)
            {
                if (additionOptionsCombo.SelectedItem == null || additionsCount.SelectedItem == null || int.Parse(additionsCount.SelectedItem.ToString()) == 0)
                {
                    DrinkSupply();
                } else
                {
                    DrinkAndAddSupply();
                }
            }
        }

        private void DrinkSupply()
        {
            Drinks drink = VendingLib.MatchDrink(coffeeOptionsCombo.SelectedItem.ToString());
            if (VendingLib.BuyProduct(drink, int.Parse(drinksCount.SelectedItem.ToString())).Equals(Status.PRODUCT_SUPPLIED))
            {
                currentDrinkSum = 0;
                priceToPay.Text = "Sum to pay is 0";
                currentBalance.Text = GetCurrentBalance();
                MessageBox.Show($"Enjoy your {coffeeOptionsCombo.SelectedItem.ToString()} :)");
            } else
            {
                MessageBox.Show("Not enough money");
            }
        }

        private void DrinkAndAddSupply()
        {
            Drinks drink = VendingLib.MatchDrink(coffeeOptionsCombo.SelectedItem.ToString());
            Additional additional = VendingLib.MatchAddition(additionOptionsCombo.SelectedItem.ToString());
            if (VendingLib.BuyProduct(drink, int.Parse(drinksCount.SelectedItem.ToString()), 
                additional, int.Parse(additionsCount.SelectedItem.ToString())).Equals(Status.PRODUCT_SUPPLIED))
            {
                currentDrinkSum = 0;
                currentAddingSum = 0;
                priceToPay.Text = "Sum to pay is 0";
                currentBalance.Text = GetCurrentBalance();
                MessageBox.Show($"Enjoy your {coffeeOptionsCombo.SelectedItem.ToString()} and {additionOptionsCombo.SelectedItem.ToString()} :)");
            }
            else
            {
                MessageBox.Show("Not enough money");
            }
        }
    }
}
